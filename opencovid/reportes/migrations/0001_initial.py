# Generated by Django 3.1 on 2020-09-02 20:54

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ReportPage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('order', models.PositiveIntegerField(db_index=True, editable=False, verbose_name='order')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('title', models.CharField(help_text='Enter Title of Report', max_length=30, unique=True)),
                ('description', models.CharField(help_text='Enter (short) description of content', max_length=128)),
                ('report_url', models.CharField(help_text='Enter a URL for report. Ex "sinadef" would generate http://opencovid-peru.com/reportes/sinadef', max_length=30, unique=True)),
            ],
            options={
                'ordering': ('order',),
                'abstract': False,
            },
        ),
    ]
