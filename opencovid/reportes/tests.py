from django.test import TestCase
from django.utils import timezone
from reportes.models import ReportPage


class ReportPageTest(TestCase):
    def setUp(self):
        ReportPage.objects.create(
            title='My Report',
            description='report description',
            slug='report'
        )

    def test_publishing_date(self):
        """Test if publishing/unpublishing updates date"""
        report = ReportPage.objects.get(title='My Report')

        self.assertEqual(report.published, False)
        self.assertIsNone(report.published_at)
        
        report.published = True
        report.save()

        self.assertEqual(report.published, True)
        self.assertIsNotNone(report.published_at)
        self.assertLess(report.published_at, timezone.now())

        report.published = False
        report.save()

        self.assertEqual(report.published, False)
        self.assertIsNone(report.published_at)


