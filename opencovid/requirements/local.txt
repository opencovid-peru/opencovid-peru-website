-r ./base.txt

pylint==2.6.0
django-debug-toolbar==2.2  # https://github.com/jazzband/django-debug-toolbar
django-extensions==3.0.8  # https://github.com/django-extensions/django-extensions
